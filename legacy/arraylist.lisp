;; This is just an experiment in implementing a stack as an array list.
;; It isn't intended to be part of any system.

;; Implementation: the first element of the array is the fill count.
;; Ignoring the fill pointer and similar features that I want to
;; implement myself.

(defun make-arraylist () (vector 0))

(defun apop (arraylist)
  (let ((count (aref arraylist 0)))
    (if (zerop count)
	(error "Cannot pop an empty array.")
	(prog1
	    (aref arraylist count)
	  ;; Not going to free the reference in this impl
	  (decf (aref arraylist 0))))))

(defun apush-fn (item arraylist)
  (let* ((index (1+ (aref arraylist 0)))
	 (array (if (array-in-bounds-p arraylist index)
		    arraylist
		    (grow-arraylist arraylist))))
    (setf (aref array index) item)
    (incf (aref array 0))
    array))

(defmacro apush (item arraylist)
  `(setf ,arraylist (apush-fn ,item ,arraylist)))

(defun grow-arraylist (arraylist)
  (let* ((dim (array-dimension arraylist 0))
	 (new-array (make-array
		     (list (* 2 dim)))))
    (copy-arraylist-contents arraylist new-array dim)
    new-array))

(defun copy-arraylist-contents (old new max)
  (loop for i from 0 upto (1- max)
     do (setf (aref new i) (aref old i))))

(defun compress-arraylist-fn (arraylist)
  (let ((new-size (expt 2 (ceiling (log (1+ (aref arraylist 0)) 2)))))
    (if (= new-size (array-dimension arraylist 0))
	arraylist
	(let ((array (make-array new-size)))
	  (copy-arraylist-contents arraylist array new-size)
	  array))))

(defmacro compress-arraylist (arraylist)
  `(setf ,arraylist (compress-arraylist-fn ,arraylist)))
