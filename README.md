# DFEXPR #

### master ###

Everything is currently in the alpha sub-dir. Build it with:

`gcc flub.c --std=c99 -falign-functions=16`

It's just a toy Lisp based on FEXPRs. The numbers.c file is a mess and is being completely rewritten on the bignums branch. `alpha/docs.org` contains the TODO list which gives an idea of where it's going.

The implementation is heavily inspired by Picolisp (http://software-lab.de/doc/ref.html). Eventually the C implementation will be replaced with a self-bootstrapping version.

### bignums ###

I am slowly and painfully adding bignum support on this branch.