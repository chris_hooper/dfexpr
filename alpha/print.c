// print.c

void fprint(VAL form)
{
  if (0 == form)
    {
      return;
    }

  if (has_type(form, SMALL_NUMBER_TYPE)) {
    if (is_negative(form)) printf("-");
    printf("%" PRIdPTR, (form & ~DESC_MASK) >> DESC_BITS);
    return;
  }

  if (has_type(form, SYMBOL_TYPE)) {
    print_symbol(form);
    return;
  }

  if (has_type(form, CONS_TYPE)) {
    print_list(form);
    return;
  }
}

void print_symbol(VAL sym)
{
  bool isString = is_symbol_in_namespace(sym, stringNamespace);
  bool isKeyword = is_symbol_in_namespace(sym, keywordNamespace);

  if (isString) printf("\"");
  if (isKeyword) printf(":");
  print_cons_string(symbol_name(sym));
  if (isString) printf("\"");
}

void print_cons_string(VAL val)
{
  if (val == NIL) {
    return;
  }
  printf("%c", (char) car(val));
  print_cons_string(cdr(val));
}

void print_list(VAL form)
{
  if (form == NIL) {
    printf("NIL");
    return;
  }

  if (car(form) == QUOTE) {
    printf("'");
    fprint(cdr(form));
    return;
  }

  printf("(");

  for(; form != NIL; form = cdr(form)) {
    if (!has_type(form, CONS_TYPE)) {
      printf(". ");
      fprint(form);
      printf(")");
      return;
    }

    fprint(car(form));
    if (cdr(form) != NIL) {
      printf(" ");
    }
  }

  printf(")");
}

VAL pr(char* str, VAL form)
{
  printf("%s ", str);
  fprint(form);
  printf("\n");
}
