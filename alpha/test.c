// test.c

VAL test_results;

bool iso(VAL x, VAL y)
{
  if (has_type(x, CONS_TYPE)) {
    if (!has_type(y, CONS_TYPE)) {
      return false;
    }

    return iso(car(x), car(y)) && iso(cdr(x), cdr(y));
  }

  return x == y;
}

void test(VAL form)
{
  VAL expected = last(form);
  VAL steps = butlast(form);
  VAL final = last(steps);
  VAL env = NIL;
  VAL result;

  for(VAL s = reverse(steps); s != NIL; s = cdr(s)) {
    result = eval(car(s), env);
  }
  if (!iso(expected, result)) {
    push(make_cons(final,
		   make_cons(expected,
			     make_cons(result, NIL))),
	 &test_results);
  }
}

bool run_tests()
{
  test_results = NIL;
  int tests_run = 0;
  FILE *file = fopen("tests.lisp", "r");

  if (NULL == file) {
    printf("Couldn't find tests.lisp.\n");
    return false;
  }

  while(!feof(file)) {
    VAL r = read(file, false);
    if (r != 0) {
      test(r);
      ++tests_run;
    }
  }

  if (test_results == NIL) {
    printf("Run %d tests...PASSED\n", tests_run);
  }
  else {
    printf("Run %d tests...%d failed.\n",
	   tests_run, length(test_results));
    for(VAL r = test_results; r != NIL; r = cdr(r)) {
      VAL form = car(car(r));
      VAL expected = car(cdr(car(r)));
      VAL result = car(cdr(cdr(car(r))));

      printf("Form {"); fprint(form);
      printf("} evaluated to {"); fprint(result);
      printf("}, expected {"); fprint(expected); printf("}.\n");
    }
  }

  printf("\n");
}
