// utils.c

bool has_type(VAL val, VAL type)
{
  if (SMALL_NUMBER_TYPE == type)
    return (val & SMALL_NUMBER_TYPE) == SMALL_NUMBER_TYPE;
  if (BIG_NUMBER_TYPE == type)
    return (val & BIG_NUMBER_TYPE) == BIG_NUMBER_TYPE;
  if (SYMBOL_TYPE == type)
    return (val & SYMBOL_TYPE) == SYMBOL_TYPE;
  if (CONS_TYPE == type)
    return  (val & DESC_MASK) == 0;

  printf("Unrecognised type.\n");
  return false;
}

void guard_type(const char* source, VAL val, VAL type)
{
  if (!has_type(val, type)) {
      printf("Expected type %s, actual %s. Source: %s\n",
	     type_to_string(type),
	     type_to_string(val & TYPE_MASK),
	     source);
    }
}

char* type_to_string(VAL type)
{
  if (has_type(type, SMALL_NUMBER_TYPE))
    return "SMALL_NUMBER";
  if (has_type(type, BIG_NUMBER_TYPE))
    return "BIG_NUMBER";
  if (CONS_TYPE == type)
    return "CONS";
  if (SYMBOL_TYPE == type)
    return "SYMBOL";

  char* str = malloc(100);
  sprintf(str, "UNRECOGNISED: %" PRIdPTR, type);
  return str;
}
