// read.c

bool is_symbol_char(char c) {
  int i = (int) c;
  if ((i >= 48 && i <= 57)
      || (i >= 65 && i <= 90)
      || (i >= 97 && i <= 122))
    {
      return true;
    }

  for(int i = 0; i < sizeof symbol_chars; ++i) {
    if (c == symbol_chars[i]) {
	return true;
      }
  }

  return false;
}

VAL read_symbol_name(FILE *file)
{
  char c = fgetc(file);

  if (is_symbol_char(c)) {
    return make_cons((VAL) c, read_symbol_name(file));
  }
  else {
    ungetc(c, file);
    return NIL;
  }
}

bool name_equal(VAL name1, VAL name2)
{
  if (name1 == NIL && name2 == NIL)
    return true;

  if (name1 == NIL || name2 == NIL)
    return false;

  if (car(name1) != car(name2))
    return false;

  return name_equal(cdr(name1), cdr(name2));
}

VAL read_symbol(FILE *file)
{
  VAL name = (VAL) read_symbol_name(file);
  bool negative = false;
  bool isNumber = true;

  if ((char) car(name) == '-') {
    negative = true;
    if (cdr(name) == NIL) {
      isNumber = false;
    }
  }

  for (VAL c = negative ? cdr(name) : name; c != NIL; c = cdr(c)) {
    if (!isdigit((char)car(c))) {
      isNumber = false;
      break;
    }
  }

  if (isNumber) {
    return make_number(name);
  }

  return intern_exploded(name, lexicalNamespace, namespace);
}

VAL read(FILE *file, bool recursive)
{
  if (!recursive) {
    lexicalNamespace = make_namespace();
  }

  chomp(file);

  if (feof(file)) {
    return 0;
  }

  char c = fgetc(file);

  if (')' == c) {
    printf("Unmatched ).\n");
    return 0;
  }

  if ('(' == c) {
    return read_list(file, false);
  }

  if ('\'' == c) {
    return read_quotation(file);
  }

  if ('"' == c)
    return read_string(file);

  if (':' == c)
    return read_keyword(file);

  if (is_symbol_char(c)) {
    ungetc(c, file);
    return read_symbol(file);
  }

  return 0;
}

VAL read_string_contents(FILE *file)
{
  if (feof(file)) {
    printf("Non-terminated string.\n");
    return 0;
  }

  char c = fgetc(file);

  if ('"' == c)
    return NIL;

  if ('\\' == c) {
    if (feof(file)) {
      printf("Non-terminated string.\n");
      return 0;
    }

    c = fgetc(file);
    if (c != '\\' && c != '"') {
      printf("Malformed escape sequence: \%c.\n", c);
      return 0;
    }
  }

  VAL rest = read_string_contents(file);
  if (rest != 0)
    return make_cons((VAL) c, rest);

  return 0;
}

VAL read_string(FILE *file)
{
  VAL contents = read_string_contents(file);

  if (contents == 0)
    return 0;

  return intern_exploded(contents, 0, stringNamespace);
}

VAL read_keyword(FILE *file)
{
  VAL contents = read_symbol_name(file);

  if (contents == 0)
    return 0;

  return intern_exploded(contents, 0, keywordNamespace);
}

VAL read_quotation(FILE *file)
{
  VAL quoted = read(file, true);
  if (0 == quoted) {
    return 0;
  }

  return make_cons(QUOTE, quoted);
}

VAL read_list(FILE *file, bool recursive)
{
  chomp(file);
  char c = fgetc(file);
  VAL elt;

  if (')' == c) {
    return NIL;
  }

  if ('.' == c) {
    if (!recursive) {
      printf("List cannot begin with .\n");
      return 0;
    }

    chomp(file);
    c = fgetc(file);

    if (')' == c) {
      printf("No CDR after dot.\n");
      return 0;
    }

    ungetc(c, file);
    elt = read(file, true);
    if (0 == elt) {
      return 0;
    }

    chomp(file);
    if (fgetc(file) != ')') {
      printf("Missing ) in dotted list.");
      return 0;
    }

    return elt;
  }

  if (-1 == c) {
    printf("Missing )\n");
    return 0;
  }

  ungetc(c, file);
  elt = read(file, true);

  if (0 == elt) {
    return 0;
  }

  VAL cons = allocate(1);
  set_car(cons, elt);

  VAL next = read_list(file, true);

  if (0 == next) {
    return 0;
  }

  set_cdr(cons, next);
  return cons;
}

void chomp(FILE *file)
{
  char c;
  while(!feof(file) && strchr("\n\t\r\f #", (c = fgetc(file))) != NULL) {
    if ('#' == c) {
      while(!feof(file) && strchr("\n\r\f", (c = fgetc(file))) == NULL);
    }
  }
  ungetc(c, file);
}
