// numbers.c

int ipow(int base, int exp)
{
    int result = 1;
    while (exp)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }

    return result;
}

VAL convert_name_to_number(VAL name, int digit)
{  
  if (name == NIL) {
    assert(digit == -1);
    return 0;
  }
  else {
    return ((car(name) - 48) * ipow(10, digit)) 
      + convert_name_to_number(cdr(name), digit - 1);
  }
}

VAL make_number(VAL digits)
{
  bool negative = false;
  if ((char) car(digits) == '-') {
    negative = true;
    digits = cdr(digits);
  }

  VAL number = convert_name_to_number(digits, length(digits) - 1);
  if (0 == number) return SMALL_NUMBER_TYPE;
  number = number << DESC_BITS;
  number += SMALL_NUMBER_TYPE;
  if (negative) number += SIGN_BIT;
  return number;
}

VAL int_to_number(VAL integer)
{
  if (integer == 0) return SMALL_NUMBER_TYPE;
  VAL original = integer;
  bool negative = integer < 0;
  if (negative) integer *= -1;
  integer = integer << DESC_BITS;
  integer += SMALL_NUMBER_TYPE;
  if (negative) integer += SIGN_BIT;
  return integer;
}

int ceil_log2(unsigned long long x)
{
  static const unsigned long long t[6] = {
    0xFFFFFFFF00000000ull,
    0x00000000FFFF0000ull,
    0x000000000000FF00ull,
    0x00000000000000F0ull,
    0x000000000000000Cull,
    0x0000000000000002ull
  };

  int y = (((x & (x - 1)) == 0) ? 0 : 1);
  int j = 32;
  int i;

  for (i = 0; i < 6; i++) {
    int k = (((x & t[i]) == 0) ? 0 : j);
    y += k;
    x >>= k;
    j >>= 1;
  }

  return y;
}

VAL negate(VAL number)
{
  number ^= SIGN_BIT;
  return number;
}

VAL is_negative(VAL number)
{
  return (number & SIGN_BIT) > 0;
}

VAL to_raw_integer(VAL number) {
  bool negative = is_negative(number);
  VAL integer = (number & ~DESC_MASK) >> DESC_BITS;
  if (negative) integer *= -1;
  return integer;
}

VAL add_small_nums(VAL x, VAL y)
{
  return int_to_number(to_raw_integer(x) + to_raw_integer(y));
}

VAL minus_small_nums(VAL x, VAL y)
{
  return int_to_number(to_raw_integer(x) - to_raw_integer(y));  
}

VAL mult_small_nums(VAL x, VAL y)
{
  return int_to_number(to_raw_integer(x) * to_raw_integer(y));
}

VAL div_small_nums(VAL x, VAL y)
{
  return int_to_number(to_raw_integer(x) / to_raw_integer(y));
}
