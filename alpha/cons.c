// cons.c

VAL car(VAL cons) {
  if (cons == NIL) return NIL;
  guard_type("car", cons, CONS_TYPE);
  return ((struct cons *)cons)->car;
}

VAL cdr(VAL cons) {
  if (cons == NIL) return NIL;
  guard_type("cdr", cons, CONS_TYPE);
  return ((struct cons *)cons)->cdr;
}

void set_car(VAL cons, VAL val) {
  guard_type("set_car", cons, CONS_TYPE);
  ((struct cons *)(cons & ~DESC_MASK))->car = val;
}

void set_cdr(VAL cons, VAL val) {
  guard_type("set_cdr", cons, CONS_TYPE);
  ((struct cons *)(cons & ~DESC_MASK))->cdr = val;
}

VAL make_cons(VAL car, VAL cdr)
{
  VAL cons = allocate();
  set_car(cons, car);
  set_cdr(cons, cdr);
  return cons;
}

int length(VAL list)
{
  int i;
  for(i = 0; list != NIL; list = cdr(list), ++i) { }
  return i;
}

VAL butlast(VAL list)
{
  if (list == NIL) return NIL;
  if (cdr(list) == NIL) return NIL;
  return make_cons(car(list), butlast(cdr(list)));
}

VAL lastcdr(VAL list)
{
  if (list == NIL) return NIL;
  if (cdr(list) == NIL) return list;
  return lastcdr(cdr(list));
}

VAL last(VAL list)
{
  return car(lastcdr(list));
}

void push(VAL item, VAL *place)
{
  *place = make_cons(item, *place);
}

VAL reverse_internal(VAL list, VAL acc)
{
  if (list == NIL) return acc;
  return reverse_internal(cdr(list), make_cons(car(list), acc));
}

VAL reverse(VAL list)
{
  return reverse_internal(list, NIL);
}
