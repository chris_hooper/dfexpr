// flub.h

typedef intptr_t VAL;
typedef VAL (*FEXPR)(VAL ops, VAL env);
typedef VAL (*DFEXPR)(VAL body, VAL ops, VAL env);

const VAL TYPE_MASK = 14;
const VAL GC_MASK = 1;
const VAL DESC_MASK = 15;
const VAL SMALL_NUMBER_TYPE = 2;
const VAL BIG_NUMBER_TYPE = 4;
const VAL SYMBOL_TYPE = 8;
const VAL CONS_TYPE = 0;
const int DESC_BITS = 4;
const VAL SIGN_BIT = 8;

struct cons {
  VAL car;
  VAL cdr;
} cons;

struct primitive
{
  char* name;
  FEXPR value;
};

void init();
void parse_test();
VAL make_symbol(VAL name);
VAL symbol_name(VAL sym);
VAL intern(char* chars, VAL lns, VAL ns);
VAL intern_exploded(VAL name, VAL lns, VAL ns);
VAL explode_string(char* str);
char* unexplode_string(VAL str);
VAL allocate();
VAL car(VAL cons);
VAL cdr(VAL cons);
void set_car(VAL cons, VAL val);
void set_cdr(VAL cons, VAL val);
VAL make_cons(VAL car, VAL cdr);
VAL read(FILE *file, bool recursive);
VAL read_list(FILE *file, bool recursive);
VAL read_quotation(FILE *file);
VAL read_string(FILE *file);
VAL read_keyword(FILE *file);
void chomp(FILE *file);
void fprint(VAL form);
VAL read_symbol(FILE *file);
VAL read_symbol_name(FILE *file);
VAL convert_name_to_number(VAL name, int digit);
void print_cons_string(VAL val);
void print_list(VAL form);
void print_symbol(VAL sym);
VAL eval(VAL form, VAL env);
VAL eval_symbol(VAL form, VAL env);
VAL eval_cons(VAL form, VAL env);
int install_primitives();
void eval_test();
char* type_to_string(VAL type);
VAL enter_env(VAL sym, VAL val, VAL env);
void exit_env(VAL current, VAL old);
VAL set_sym_val(VAL sym, VAL val);
VAL get_sym_val(VAL sym);
void guard_type(const char* source, VAL val, VAL type);
int length(VAL list);
void top_level();
bool name_equal(VAL name1, VAL name2);

VAL lquote(VAL form, VAL env);
VAL lplus(VAL args, VAL env);
VAL lminus(VAL args, VAL env);
VAL lmult(VAL args, VAL env);
VAL ldivis(VAL args, VAL env);
VAL ldo(VAL args, VAL env);
VAL lcar(VAL args, VAL env);
VAL lcdr(VAL args, VAL env);
VAL lcons(VAL args, VAL env);
VAL lvau(VAL body, VAL args, VAL env);
VAL llambda(VAL body, VAL args, VAL env);
VAL leval(VAL args, VAL env);
VAL lprint(VAL args, VAL env);
VAL lif(VAL args, VAL env);
VAL lequals(VAL args, VAL env);
VAL llength(VAL args, VAL env);
VAL lquit(VAL args, VAL env);
VAL lset(VAL args, VAL env);
VAL lno(VAL args, VAL env);
VAL lload(VAL args, VAL env);
VAL lconsp(VAL args, VAL env);
VAL lsymbolp(VAL args, VAL env);
VAL lnumberp(VAL args, VAL env);

VAL NIL;
VAL QUOTE;
VAL T;
VAL namespace;
VAL stringNamespace;
VAL lexicalNamespace;
VAL keywordNamespace;
VAL namespaceSymbol;

const char symbol_chars[] = {'-', '+', '*', '/', '_', '$', '='};
struct cons *memory;
struct cons *maxMemory;
intptr_t maxSmallNumber;
bool looping;
int primitiveCount;
