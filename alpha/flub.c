// flub.c

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <inttypes.h>
#include <stdbool.h>
#include <errno.h>
#include <assert.h>
#include <math.h>

#include "flub.h"
#include "utils.c"
#include "numbers.c"
#include "symbols.c"
#include "cons.c"
#include "read.c"
#include "print.c"
#include "eval.c"
#include "primitives.c"
#include "test.c"

void main()
{
  init();
  run_tests();

  //parse_test();
  //eval_test();
  top_level();
}

void init()
{
  maxSmallNumber = INTPTR_MAX >> DESC_BITS;

  // Init memory management
  int heapSize = sizeof(struct cons) * 1000000;
  intptr_t mem = (intptr_t) malloc(heapSize);
  maxMemory = (struct cons*) (mem + heapSize);
  while ((mem % 64) != 0) {
    ++mem;
  }
  memory = (struct cons*) mem;

  // Init symbols and namespaces
  VAL nilcons = make_cons(0, 0);
  NIL = (nilcons | SYMBOL_TYPE);
  set_sym_val(NIL, NIL);

  VAL lispNamespace = make_namespace();
  stringNamespace = make_namespace();
  keywordNamespace = make_namespace();

  VAL nilName = symbol_name(intern("NIL", 0, stringNamespace));
  set_car(nilcons, nilName);
  ns_push(NIL, lispNamespace);

  // Check that NIL was constructed correctly.
  guard_type("init", NIL, SYMBOL_TYPE);
  assert(NIL == intern("NIL", 0, lispNamespace));
  assert(get_sym_val(NIL) == NIL);
  assert(symbol_name(intern("NIL", 0, stringNamespace)) ==
	 symbol_name(intern("NIL", 0, lispNamespace)));

  QUOTE = intern("QUOTE", 0, lispNamespace);

  T = intern("T", 0, lispNamespace);
  set_sym_val(T, T);

  primitiveCount = install_primitives(lispNamespace);
  set_sym_val(intern("lisp", 0, lispNamespace), lispNamespace);
  namespace = lispNamespace;
  namespaceSymbol = intern("lisp", 0, lispNamespace);

  printf("DFEXPR Alpha System\n");
  printf("%d-bit version\n", ceil_log2(UINTPTR_MAX));
  printf("Max small number size: %" PRIdPTR "\n", maxSmallNumber);

  printf("Installed %d primitives: ", primitiveCount);
  for(VAL rest = cdr(namespace); rest != NIL; rest = cdr(rest)) {
    fprint(car(rest));
    printf(" ");
  }

  printf("Loading prologue.lisp\n");
  VAL prologue = intern("prologue.lisp", 0, stringNamespace);
  lload(make_cons(prologue, NIL), NIL);
}

int install_primitives(VAL ns)
{
  int count = sizeof primitives / sizeof (struct primitive);
  for(int i = 0; i < count; ++i) {
    VAL sym = intern(primitives[i].name, 0, ns);
    void* addr = primitives[i].value;
    assert(((intptr_t) addr) % 16 == 0);
    VAL num = (VAL) (addr + SMALL_NUMBER_TYPE);
    set_sym_val(sym, num);
  }

  return count;
}

void top_level()
{
  printf("Starting top level\n\n");

  looping = true;
  VAL env = NIL;

  while(true) {
    printf("> ");
    VAL form = read(stdin, false);
    VAL result = eval(form, env);
    if (!looping) {
      printf("Exiting top level.\n");
      break;
    }
    fprint(result);
    printf("\n");
  }
}

VAL allocate()
{
  struct cons *ptr = (struct cons*) memory;
  memory += sizeof(struct cons);
  if (memory >= maxMemory) printf("Out of heap space\n");
  return (VAL) ptr;
}

void parse_test()
{
  FILE *file = fopen("text.lisp", "r");

  if (NULL == file) {
    printf("Couldn't find text.lisp.\n");
    return;
  }

  while(!feof(file)) {
    fprint(read(file, false));
    printf("\n");
  }
}
