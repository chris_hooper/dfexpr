// symbols.c

VAL get_sym_val(VAL sym)
{
  guard_type("get_sym_val", sym, SYMBOL_TYPE);
  return cdr(sym & ~DESC_MASK);
}

VAL set_sym_val(VAL sym, VAL val)
{
  guard_type("set_sym_val", sym, SYMBOL_TYPE);
  set_cdr(sym & ~DESC_MASK, val);
  return val;
}

VAL symbol_name(VAL sym)
{
  guard_type("symbol_name", sym, SYMBOL_TYPE);
  return car(sym & ~DESC_MASK);
}

bool is_symbol_in_namespace(VAL sym, VAL ns)
{
  guard_type("is_symbol_in_namespace", sym, SYMBOL_TYPE);
  guard_type("is_symbol_in_namespace", ns, CONS_TYPE);

  for(VAL rest = cdr(ns); rest != NIL; rest = cdr(rest)) {
    if (car(rest) == sym) { return true; }
  }

  return false;
}

VAL make_symbol(VAL name)
{
  VAL sym = make_cons(name, NIL);
  sym += SYMBOL_TYPE;
  return sym;
}

VAL make_namespace()
{
  return make_cons(NIL, NIL);
}

VAL ns_push(VAL sym, VAL ns)
{
  set_cdr(ns, make_cons(sym, cdr(ns)));
}

VAL copy_namespace(VAL ns)
{
  VAL new = make_namespace();
  for(ns = cdr(ns); ns != NIL; ns = cdr(ns)) {
    ns_push(car(ns), new);
  }
  return new;
}

VAL find_in_namespace(VAL name, VAL ns)
{
  for(VAL rest = cdr(ns); rest != NIL; rest = cdr(rest)) {
    if (name_equal(name, symbol_name(car(rest)))) {
      return car(rest);
    }
  }
  return 0;
}

VAL add_to_namespace(VAL name, VAL ns, VAL val)
{
  VAL sym = make_symbol(name);
  set_sym_val(sym, val == 0 ? sym : val);

  ns_push(sym, ns);

  return sym;
}

VAL find_or_add(VAL name, VAL ns, VAL val)
{
  VAL sym = find_in_namespace(name, ns);
  if (sym == 0) return add_to_namespace(name, ns, val);
  return sym;
}

VAL intern(char* chars, VAL lns, VAL ns)
{
  VAL name = explode_string(chars);
  return intern_exploded(name, lns, ns);
}

VAL intern_exploded(VAL name, VAL lns, VAL ns)
{
  VAL str = find_or_add(name, stringNamespace, 0);

  if (ns == stringNamespace) return str;
  if (ns == keywordNamespace) return find_or_add(name, ns, 0);

  VAL sym = 0;
  if (lns != 0) sym = find_in_namespace(name, lns);
  if (sym != 0) return sym;

  sym = find_in_namespace(name, ns);
  if (sym != 0) return sym;

  if (lns == 0) return add_to_namespace(name, ns, NIL);
  return add_to_namespace(name, lns, NIL);
}

VAL make_global(VAL sym)
{
  VAL name = symbol_name(sym);
  for(VAL n = cdr(namespace); n != NIL; n = cdr(n)) {
    if (name_equal(name, symbol_name(car(n)))) {
      return car(n);
    }
  }

  ns_push(sym, namespace);
  return sym;
}

VAL explode_string(char* str)
{
  if (*str == '\0') {
    return NIL;
  }
  else {
    VAL cons = allocate(1);
    set_car(cons, *str);
    set_cdr(cons, explode_string(++str));
    return cons;
  }
}

char* unexplode_string(VAL str)
{
  if (str == NIL) return "";
  char* result = malloc(length(str) + 1);
  int i;
  for(i = 0; str != NIL; str = cdr(str), ++i) {
    result[i] = (char) car(str);
  }
  result[i] = '\0';
  return result;
}
