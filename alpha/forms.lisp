"A string"

NIL
123
'123
''123
0
-0
-1
-123

'"Quoted string"
''"Double quoted string"
x
y

(+ 1 2 3)
(+ (+ 1 2) (+ 3 4))
(- 10 3 5)
(* 2 3 4)
(/ 40 4 2)

(- 10 -1)
(- -10 1)
(+ -1 -2)
(* -1 5)
(/ -10 2 1)
(/ -10 2 -1)

(do (+ 1 2)
    (- 7 6)
  (/ 8 4)
  (* 6 8))

(car '(1 2 3))
(cdr '(1 2 3))
(cons 1 '(2 3))
(cons 1 2)

"(4)" ('(vau x x) 4)
"4" ('(vau (x) x) 4)
"5" ('(vau (x) (+ 1 x)) 4)
"3" ('(vau (x) (x 1)) (vau (y) (+ 2 y)))
"3" ('(vau (x) (x 1)) (vau (x) (+ 2 x)))
"15" ('(vau (x y) (+ x y)) 7 8)
"NIL" x
"NIL" y

(print '(1 2 3))
(eval '(+ 1 2 3))

"NIL" (= 1 2)
"T" (= 1 1)
"NIL" (= '(1 2 3) '(1 2 3))
"T" (= 'x 'x)
"NIL" (= 'x 'y)

(if (= 1 1) T NIL)
(if (= 1 2) NIL T)
(if (= 1 1) (print "correct") (print "wrong"))
(if (= 1 2) (print "wrong") (print "correct"))

(length NIL)
(length '(1))
(length '(1 2 3))
