// eval.c

VAL eval(VAL form, VAL env)
{
  if (form == 0) {
    return 0;
  }

  if (has_type(form, SMALL_NUMBER_TYPE)) {
    return form;
  }

  if (has_type(form, SYMBOL_TYPE)) {
    return eval_symbol(form, env);
  }

  if (has_type(form, CONS_TYPE)) {
    return eval_cons(form, env);
  }
}

VAL eval_symbol(VAL form, VAL env)
{
  return get_sym_val(form);
}

VAL eval_cons(VAL form, VAL env)
{
  VAL op = eval(car(form), env);

  if (NIL == op) {
    printf("Not recognised: "); fprint(car(form)); printf("\n");
    return 0;
  }

  if (has_type(op, SYMBOL_TYPE)) {
    op = eval_symbol(op, env);
  }

  if (op == 0) return 0;

  if (has_type(op, SYMBOL_TYPE)) {
    printf("Cannot apply symbol to arguments.\n");
    return 0;
  }

  if (has_type(op, SMALL_NUMBER_TYPE)) {
    FEXPR addr = (FEXPR) (op & ~DESC_MASK);
    return addr(cdr(form), env);
  }

  if (has_type(op, CONS_TYPE)) {
    VAL fn = eval(car(op), env);
    if (fn == 0) return 0;

    // TODO: recursive solution where fn may itself be non-primitive.
    guard_type("eval_cons", fn, SMALL_NUMBER_TYPE);

    DFEXPR addr = (DFEXPR) (fn & ~DESC_MASK);
    return addr(cdr(op), cdr(form), env);
  }

  return 0;
}

VAL eval_args(VAL args, VAL env)
{
  if (args == NIL) return NIL;

  return make_cons(eval(car(args), env), eval_args(cdr(args), env));
}

VAL enter_env(VAL sym, VAL val, VAL env)
{
  guard_type("enter_env", sym, SYMBOL_TYPE);
  env = make_cons(make_cons(sym, get_sym_val(sym)), env);
  set_sym_val(sym, val);
  return env;
}

void exit_env(VAL current, VAL old)
{
  if (current == old) {
    return;
  }

  VAL binding = car(current);
  set_sym_val(car(binding), cdr(binding));

  exit_env(cdr(current), old);
}

VAL pred(bool condition)
{
  if (condition) return T;
  return NIL;
}
