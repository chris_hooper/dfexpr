# Put fns that can be defined in Lisp here.

(var 'map '(lambda (f list)
	    (if (no list) NIL (cons (f (car list)) (map f (cdr list))))))
