// primitives.c

VAL lquote(VAL args, VAL env)
{
  return args;
}

VAL lplus(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  int result = int_to_number(0);

  for(; pars != NIL; pars = cdr(pars)) {
    result = add_small_nums(result, car(pars));
  }

  return result;
}

VAL lmult(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  int result = int_to_number(1);

  for(; pars != NIL; pars = cdr(pars)) {
    result = mult_small_nums(result, car(pars));
  }

  return result;
}

VAL lminus(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  int result = car(pars);

  for(pars = cdr(pars); pars != NIL; pars = cdr(pars)) {
    result = minus_small_nums(result, car(pars));
  }

  return result;
}

VAL ldivis(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  int result = car(pars);

  for(pars = cdr(pars); pars != NIL; pars = cdr(pars)) {
    result = div_small_nums(result, car(pars));
  }

  return result;
}

VAL ldo(VAL args, VAL env)
{
  while(true) {
    VAL result = eval(car(args), env);
    args = cdr(args);
    if (args == NIL) return result;
  }
}

VAL lcar(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  if (cdr(pars) != NIL) {
    printf("Too many args to CAR.\n");
    return 0;
  }

  return car(car(pars));
}

VAL lcdr(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  if (cdr(pars) != NIL) {
    printf("Too many args to CDR.\n");
    return 0;
  }

  return cdr(car(pars));
}

VAL lcons(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  if (cdr(cdr(pars)) != NIL) {
    printf("Too many args to CONS.\n");
    return 0;
  }

  return make_cons(car(pars), (car(cdr(pars))));
}

VAL docall(VAL formal, VAL envParm, VAL body, VAL args, VAL env)
{
  VAL newEnv = env;

  if (formal != NIL) {
    if (has_type(formal, SYMBOL_TYPE)) {
      newEnv = enter_env(formal, args, env);
    }
    else {
      for(; formal != NIL; formal = cdr(formal), args = cdr(args)) {
	if (car(formal) != NIL) {
	  newEnv = enter_env(car(formal), car(args), newEnv);
	}
      }
    }
  }

  if (envParm != NIL) newEnv = enter_env(envParm, env, newEnv);
  VAL result = ldo(body, newEnv);
  exit_env(newEnv, env);
  return result;
}

VAL lvau(VAL body, VAL args, VAL env)
{
  VAL formal = car(body);
  VAL envParm = car(cdr(body));
  VAL forms = cdr(cdr(body));
  return docall(formal, envParm, forms, args, env);
}

VAL llambda(VAL body, VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  return docall(car(body), NIL, cdr(body), pars, env);
}

VAL leval(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  env = cdr(pars) != NIL ? car(cdr(pars)) : env;
  return eval(car(pars), env);
}

VAL lprint(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  fprint(car(pars));
  return car(pars);
}

VAL lif(VAL args, VAL env)
{
  if (eval(car(args), env) != NIL) {
    return eval(car(cdr(args)), env);
  }
  return eval(car(cdr(cdr(args))), env);
}

VAL lequals(VAL args, VAL env)
{
  VAL x = eval(car(args), env);
  VAL y = eval(car(cdr(args)), env);

  return pred(x == y);
}

VAL llength(VAL args, VAL env)
{
  VAL list = eval(car(args), env);
  if (!has_type(list, CONS_TYPE) && list != NIL) {
    printf("Invalid argument passed to length.\n");
    return 0;
  }

  return (length(list) << DESC_BITS) + SMALL_NUMBER_TYPE;
}

VAL lquit(VAL args, VAL env)
{
  looping = false;
  return NIL;
}

VAL lset(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  VAL var= car(pars);
  VAL val = car(cdr(pars));

  guard_type("lset", var, SYMBOL_TYPE);
  set_sym_val(var, val);
  return val;
}

VAL lno(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  return pred(car(pars) == NIL);
}

VAL lload(VAL args, VAL env)
{
  char* filename = unexplode_string(symbol_name(eval(car(args), env)));
  FILE *file = fopen(filename, "r");

  if (NULL == file) {
    printf("Couldn't find forms.lisp.\n");
    return NIL;
  }

  while(!feof(file)) {
    eval(read(file, false), env);
  }

  return T;
}

VAL lconsp(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  pred(has_type(car(pars), CONS_TYPE));
}

VAL lsymbolp(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  pred(has_type(car(pars), SYMBOL_TYPE));
}

VAL lnumberp(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  pred(has_type(car(pars), SMALL_NUMBER_TYPE));
}

VAL lintern(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  intern_exploded(symbol_name(car(pars)), 0, namespace);
}

VAL lsymbol_name(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  return intern_exploded(symbol_name(car(pars)), 0, stringNamespace);
}


VAL lvar(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  VAL result = NIL;
  for (VAL p = pars; p != NIL; p = cdr(cdr(p))) {
    result = set_sym_val(make_global(car(p)), car(cdr(p)));
  }
  return result;
}

VAL lsymbols(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);

  if (pars == NIL) return namespaceSymbol;

  VAL previous = namespaceSymbol;
  namespaceSymbol = car(pars);

  if (cdr(pars) != NIL) {
    make_global(car(pars));
    VAL new = make_namespace();
    set_sym_val(namespaceSymbol, new);

    for (VAL p = cdr(pars); p != NIL; p = cdr(p)) {
      if (car(p) != NIL) {
	for (VAL ns = cdr(get_sym_val(car(p))); ns != NIL; ns = cdr(ns)) {
	  VAL sym = car(ns);
	  if (0 == find_in_namespace(symbol_name(sym), new)) {
	    ns_push(sym, new);
	  }
	}
      }
    }
  }

  namespace = get_sym_val(namespaceSymbol);
  return previous;
}

VAL lfind(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  VAL result;

  if (cdr(args) == NIL) {
    result = find_in_namespace(symbol_name(car(pars)), namespace);
  }
  else {
    result = find_in_namespace(symbol_name(car(cdr(pars))), get_sym_val(car(pars)));
  }

  return result == 0 ? NIL : result;
}

VAL ldebug(VAL args, VAL env)
{
  VAL pars = eval_args(args, env);
  VAL p = car(pars);
  printf("Raw value: %ld\n", p);
  printf("Data: %ld\n", p & ~DESC_MASK);
  printf("Type: %s\n", type_to_string(p & TYPE_MASK));
  printf("Sign bit: %ld\n", p & SIGN_BIT);
  return car(pars);
}

struct primitive primitives[] = {
  { "QUOTE", lquote },
  { "+", lplus },
  { "-", lminus },
  { "*", lmult },
  { "/", ldivis },
  { "do", ldo },
  { "car", lcar },
  { "cdr", lcdr },
  { "cons", lcons },
  { "vau", (FEXPR) lvau },
  { "print", lprint },
  { "eval", leval },
  { "if", lif },
  { "=", lequals },
  { "length", llength },
  { "quit", lquit },
  { "lambda", (FEXPR) llambda },
  { "set", lset },
  { "no", lno },
  { "load", lload },
  { "consp", lconsp },
  { "symbolp", lsymbolp },
  { "numberp", lnumberp },
  { "intern", lintern },
  { "symbol-name", lsymbol_name },
  { "var", lvar },
  { "symbols", lsymbols },
  { "find", lfind },
  { "debug", ldebug }
};
