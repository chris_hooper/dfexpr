abc    123

456abc

(1 2 3)(1 2 3) (1 2 3) ( 1 2 3 )

1 -1 0 -0 123 -123

(abc 123 456abc)
(abc
 123
 456abc)

()
(  )

(1 . 2)
(1 2 3 . 4
   )

'(1 2 3)
'abc
'6
'()

"hello"
"123"
"abc"
""
"string after empty string"
